'use strict';

angular.module('testLogin', []).
  service('loginSvc', loginSvc).
  factory('tokenInjector', tokenInjector).
  directive('login', function() {
    return {
      templateUrl: 'html/login.html',
      controller: loginCtrl,
      controllerAs: 'login'
    }
  });

loginCtrl.inject = ['loginSvc', 'netSvc', 'infoSvc'];
tokenInjector.inject = ['$loginSvc'];

function loginSvc() {
  var _token = '';
  return {
    setToken: function(token) {
      _token = token;
    },
    getToken: function() {
      return _token;
    }
  }
}
function loginCtrl(loginSvc, netSvc, infoSvc) {
  var self = this;
  this.isSign = function() {
    return loginSvc.getToken();
  }
  this.register = function() {
    request('register');
  }
  this.login = function() {
    request('login');
  }
  this.logout = function() {
    loginSvc.setToken('');
  }
  function request(type) {
    if (!(self.pass && self.user)) {
      infoSvc.setText('type username and password');
      return;
    }
    netSvc.post(
      type + '/',
      {
        'username': self.user,
        'password': self.pass
      },
      function(response) {
        if (!response.data) return;
        if (!response.data.success) {
          infoSvc.setText(response.data.message);
          return;
        }
        loginSvc.setToken(response.data.token);
      }
    );
  }
}
function tokenInjector(loginSvc) {
  return {
    request: function(config) {
      if (loginSvc.getToken()) config.headers['Authorization'] = 'Token ' + loginSvc.getToken();
      return config;
    }
  }
}