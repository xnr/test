'use strict';

angular.module('testList', []).
  controller('listCtrl', listCtrl).
  service('listSvc', listSvc);

listCtrl.inject = ['listSvc'];
listSvc.inject = ['netSvc'];

function listCtrl(listSvc) {
  var self = this;
  listSvc.get(function(products) {
    self.products = products;
  })
}
function listSvc(netSvc) {
  var _products = [];
  function get(callback) {
    if (_products.length) {
      callback(_products);
      return;
    }
    netSvc.get('products/', function(response) {
      if (!response.data) return;
      response.data.forEach(function(product) {
        _products.push(product);
      });
      callback(_products);
    })
  }
  return {get: get}
}