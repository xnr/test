'use strict';

angular.module('testMain', ['ngRoute', 'testList', 'testProduct', 'testLogin', 'testInfo']).
  value('products', []).
  service('toListSvc', toListSvc).
  directive('toListButton', toListButton).
  service('netSvc', netSvc).
  config(config);

config.inject = ['$routeProvider', '$httpProvider'];
toListSvc.inject = ['$rootScope'];
netSvc.inject = ['$http', 'infoSvc'];

function config($routeProvider, $httpProvider) {
  $httpProvider.interceptors.push('tokenInjector');
  $routeProvider.
    when('/list', {
      templateUrl: 'html/list.html',
      controller: 'listCtrl',
      controllerAs: 'list'
    }).
    when('/product/:productId', {
      templateUrl: 'html/product.html',
      controller: 'productCtrl',
      controllerAs: 'product'
    }).
    otherwise({
      redirectTo: '/list'
    });
}
function toListSvc($rootScope) {
  return {
    leaveList: function() {
      $rootScope.$broadcast('leaveList');
    }
  }
}
function toListButton() {
  return function(scope, elem, attrs) {
    scope.$on('leaveList', toggleVisible);
    elem.on('click', toggleVisible);
    function toggleVisible() {
      elem.toggleClass('invisible');
    }
  }
}
function netSvc($http, infoSvc) {
  var apiUrl = 'http://smktesting.herokuapp.com/api/';
  function netError() {
    infoSvc.setText('request failed');
  }
  return {
    get: function(source, callback) {
      $http.get(apiUrl + source).then(
        callback,
        netError
      );
    },
    post: function(source, data, callback) {
      $http.post(apiUrl + source, data, config).then(
        callback,
        netError
      );
    }
  }
}