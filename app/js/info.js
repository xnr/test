'use strict';

angular.module('testInfo', []).
  service('infoSvc', infoSvc).
  directive('info', function() {
    return {
      templateUrl: 'html/info.html',
      controller: infoCtrl,
      controllerAs: 'info'
    }
  });

infoSvc.inject = ['$rootScope'];
infoCtrl.inject = ['$rootScope', 'infoSvc'];

function infoSvc($rootScope) {
  var _text = '';
  return {
    setText: function(text) {
      _text = text;
      $rootScope.$broadcast('getInfo');
    },
    getText: function() {
      return _text;
    }
  }
}
function infoCtrl($rootScope, infoSvc) {
  var self = this;
  this.text = '';
  $rootScope.$on('getInfo', function() {
    self.text = infoSvc.getText();
  });
}