'use strict';

angular.module('testProduct', []).
  controller('productCtrl', productCtrl);

productCtrl.inject = ['$routeParams', 'listSvc', 'loginSvc', 'infoSvc', 'toListSvc', 'netSvc'];

function productCtrl($routeParams, listSvc, loginSvc, infoSvc, toListSvc, netSvc) {
  var self = this;
  this.reviews = [];
  this.review = {
    rate: 0,
    setRate: function(rate) {
      self.review.rate = rate;
      [].forEach.call(document.getElementsByClassName('star-rate-star'), function(item) {
        angular.element(item).removeClass('rated');
      });
      angular.element(document.getElementById('star' + rate)).addClass('rated');
    }
  };
  this.review.submit = function() {
    if (!loginSvc.getToken()) {
      infoSvc.setText('you must login to review');
      return;
    }
    if (!self.review.text || !self.review.rate) {
      infoSvc.setText('add rating and some text to review');
      return;
    }
    netSvc.post(
      'reviews/' + $routeParams.productId,
      {
        'rate': self.review.rate,
        'text': self.review.text
      },
      function() {
        netSvc.get('reviews/' + $routeParams.productId, getReviews);
      }
    );
  }
  listSvc.get(function(products) {
    self.detail = products.filter(function(product) {
      return product.id == $routeParams.productId;
    });
  });
  netSvc.get('reviews/' + $routeParams.productId, getReviews);
  toListSvc.leaveList();
  function getReviews(response) {
    if (!response.data) return;
    response.data.forEach(function(review) {
      self.reviews.push(review);
    });
  }
}